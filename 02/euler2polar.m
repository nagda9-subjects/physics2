% Convert Euler equation to polar 
function [c, s]=euler2polar(theta)
c=cos(theta);
if theta<0
    s=-1i*sin(abs(theta));
else
    s=1i*sin(abs(theta));
end
end