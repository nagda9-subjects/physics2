% Wave-function exact solver
function [Re,Im]=Psi(nx,Enx,time,x) 
[cosine, sine]=euler2polar(-Enx*time);
Re=sqrt(2)*cosine.*sin(nx*pi*x);
Im=sqrt(2)*sine.*sin(nx*pi*x);
end
