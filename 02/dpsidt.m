%creating the differential function
function [dpsi]=dpsidt(psi,dx)
len=length(psi);
dpsi=zeros(1,len);
for j=2:len-1
    dpsi(j)=0.5i*((psi(j+1)-2*psi(j)+psi(j-1))/dx^2);
end
dpsi(1)=0.5i*((psi(3)-2*psi(2)+psi(1))/dx^2);             % For left boundary
dpsi(len)=0.5i*((psi(len)-2*psi(len-1)+psi(len-2))/dx^2); % For right boundary
end