function [M]=getmovie(N)
h = figure;
ax = gca;
ax.NextPlot = 'replaceChildren';
h.Visible = 'off';
% We will need a spacial and a time domain too: 
% for stability (and ease of adjustability dt is dependent on dx):

dx=0.05;                  
x=0:dx:1;                 % Space
C=0.01;                   % Arbitrary number for stability (adjust!)
dt=C*dx^2;                
t=0:dt:1;                 % Time
dpsi=zeros(1,length(x));  % Derivative of psi (time)

% A few "keep-in-minds":
% Initial conditions - where t(1)=0
% Because we would have infinite energy if this was not true. 
% Psi is continous!
% For stability (and simplicity): h-bar, and m are both equal to 1!!!

nx=N; % Energy Level
Enx=0.5*(nx*pi)^2;
[Re2,Im2]=Psi(nx,Enx,t(1),x);
psi=Re2+Im2;
% Solving wave function as ODE.
% Solution is based on Finite-Difference Method and Runge Kutta-3.

% Some preformating on the plot:
axis([0 1 -2.5 2.5]);
xlabel('Spatial plane (the infinite potential well)');
hold on; grid on; axis manual;

M=struct('cdata',[],'colormap',[]); %the movie

for i=1:length(t)
    % Boundaries: 
    psi(1)=0; psi(length(x))=0;
    % RK-3:                    
    dpsi=dpsidt(psi,dx);
    k1=dt*dpsi;
    
    dpsi2=dpsidt(psi+k1/2,dx);
    k2=dt*dpsi2;
    
    dpsi3=dpsidt(psi+k2/2,dx);
    k3=dt*dpsi3;
        
    psi=psi+(1/6)*k1+(2/3)*k2+(1/6)*k3;
%   Check for divergence    
%     if max(abs(psi)) > 1.5   
%         sprintf('Error!!! At i=%d',i)
%         return
%     end
    
    % Plotting solution (not for every frame for reasonable runtime)
    if rem(i,10/dx)==0 
        % Caluclating wave function:
        PSI_PLOT=plot(x,real(psi),'r');
   
        % Calculating energy density:
        rho=real(psi).^2+imag(psi).^2;
        E=plot(x,rho,'b');
    
        legend('Wave function (\Psi)','Energy density (\rho)');
        Time_text=sprintf('At t = %.3f',t(i));
        time=text(0.02,2,Time_text);
        M(length(M)+1)=getframe;
        drawnow; delete(PSI_PLOT); delete(time); delete(E);
    end
    %Special case for first frame:
    if i==1
        PSI_PLOT=plot(x,real(psi),'r');
        rho=real(psi).^2+imag(psi).^2;
        E=plot(x,rho,'b');
        legend('Wave function (\Psi)','Energy density (\rho)');
        Time_text=sprintf('At t = %.3f',t(i));
        time=text(0.02,2,Time_text);
        M(1)=getframe;
        drawnow; delete(PSI_PLOT); delete(time); delete(E);
    end
    
end
end