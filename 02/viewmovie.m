function [] = viewmovie(M)
h=figure;
h.Visible = 'on'; %Making sure that the movie will be visible.
title('The time-dependent wave function and the energy density function');
xlabel('Spatial plane (the infinite potential well)');
movie(M,1); %Plays the movies once.
end

