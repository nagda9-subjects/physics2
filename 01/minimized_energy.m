clear all; close all; clc;

% constans
eps0 = 8.8542E-12;          % vacuum permitivity
qp = 1.602E-19;             % proton charge
qe = -qp;                   % electron charge

h_bar = 1.054572E-34;       % reduced Planck constant
m_e = 9.109E-31;            % mass of electron
c = -(h_bar)^2/(2.0*m_e);   % The value of -(h_bar^2)/2m

TotalLength = 2E-9;       % interval length
PrSize = 500;               % precision

%Height of the potential wall confining the electron
Wallenergy = 1E10;
WallWidth = 100;

x = linspace(-TotalLength, TotalLength, PrSize);
dx = x(2)-x(1);

max_h_shift = 10;
max_dist_shift = 15;

E_proton_vect = zeros(max_h_shift, max_dist_shift);
E0_vect = zeros(max_h_shift, max_dist_shift);
E1_vect = zeros(max_h_shift, max_dist_shift);
E_total = zeros(max_h_shift, max_dist_shift);

psi0_vect = zeros(max_h_shift, max_dist_shift, PrSize);
psi1_vect = zeros(max_h_shift, max_dist_shift, PrSize);

V_vect = zeros(max_h_shift, max_dist_shift, PrSize);
distances = zeros(max_h_shift, max_dist_shift);

for n = 1:max_h_shift % eltolom a baloldali H atomot a gödör felétől balra
    for m = 1:max_dist_shift % növelem a jobboldali H atom távolságát a másik H-tól
        Hpos = [PrSize/2-max_h_shift+2*(n-1) PrSize/2+m];
        distances(n, m) = abs(Hpos(2)-Hpos(1));

        for j=1:PrSize
            if j~=Hpos(1) && j~=Hpos(2)
                V(j)=1/(4*pi*eps0)*qe*qp/(abs(j-Hpos(1))*dx)+1/(4*pi*eps0)*qe*qp/(abs(j-Hpos(2))*dx);
            else
                V(j)=min(V);
            end
        end
        V(1:WallWidth) = Wallenergy;
        V(PrSize-WallWidth:PrSize) = Wallenergy;

        E_proton = 1/(4*pi*eps0)*qp*qp/(abs(Hpos(1)-Hpos(2))*dx);
        E_proton_vect(n, m) = E_proton;
        V_vect(n, m, :) = V;

        %Now it builds up the Hamiltonian
        H = zeros(PrSize,PrSize);

        for i = 2 : PrSize-1
            H(i,i)   = -2.0*c/dx^2 + V(i);
            H(i,i-1) = c/dx^2;
            H(i,i+1) = c/dx^2;  
        end

        %The sides of the matrix:  

        %Left side:  Psi(0)=0, forward difference approximation 
        H(1,1) = c/dx^2 + V(1);
        H(1,2) = -2.0*c/dx^2;
        H(1,3) = c/dx^2;

        %Right side: Psi(PrSize+1)=0
        H(PrSize,PrSize)   = c/dx^2 + V(PrSize);
        H(PrSize,PrSize-1) =-2.0*c/dx^2;
        H(PrSize,PrSize-2) = c/dx^2;

        %Now it solves the eigenvalue-problem using the built-in solver of Matlab
        [Psi,Emx] = eig(H);

        %Makes a one-dimensional list from the energy matrix
        for i = 1:PrSize
           E(i) = Emx(i,i);
        end

        %Finds the minimum-energy eigenvalue
        [MinE,MinEindex] = min(E);

        Psi0 = Psi(:,MinEindex);
        Psi0 = Psi0./sqrt(sum(abs(Psi0).^2*dx));
        psi0_vect(n, m, :) = Psi0;
        E0 = MinE;
        E0_vect(n, m) = E0;

        %Finds the second minimum-energy eigenvalue
        E(MinEindex) = E(MinEindex)+Wallenergy;
        [MinE,MinEindex] = min(E);
        MinE;

        Psi1 = Psi(:,MinEindex);
        Psi1 = Psi1./sqrt(sum(abs(Psi1).^2*dx));
        psi1_vect(n, m, :) = Psi1;
        E1 = MinE;
        E1_vect(n, m) = E1;
    end
end 
E_total = E0_vect + E1_vect + E_proton_vect;
E0_vect
%% Molecule Energy 

%The lowest energy molecule
E_total_lin = reshape(E_total, n*m, 1);
[~, ind] = max(E_total_lin) ;
[row, col] = ind2sub([n m], ind);
E_total(row, col);

bondlength = (distances(row, col))*dx;

% az optimális pozícióhoz tartozó Energiák távolságok alapján 
figure;
plot(distances(row,:).*dx, E_total(row,:));
title('Molecule Energy');
xlabel('Distance (m)')
ylabel('Molecule Energy (eV)');

figure;
subplot(2,1,1)
plot(x(1:PrSize),V(1:PrSize),'r--');
hold on;
plot(x,squeeze(psi0_vect(row, col, :)), 'r');
plot(x,squeeze(psi1_vect(row, col, :)), 'b');
title('\psi');
axis ([-1.5E-9 1.5E-9 -2E5 4E5]);

subplot(2,1,2)
plot(x(1:PrSize),V(1:PrSize),'r--');
hold on;
plot(x,squeeze(abs(psi0_vect(row, col, :)).^2), 'r');
plot(x,squeeze(abs(psi1_vect(row, col, :)).^2), 'b');
title('|\psi|^{2}');
axis ([-0.5E-9 0.5E-9 0 4E10]);

figure;
plot(V(200:300))

