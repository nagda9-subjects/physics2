clear all; close all; clc;

% constans
eps0 = 8.8542E-12;          % vacuum permitivity
qp = 1.602E-19;             % proton charge
qe = -qp;                   % electron charge

h_bar = 1.054572E-34;       % reduced Planck constant
m_e = 9.109E-31;            % mass of electron
c = -(h_bar)^2/(2.0*m_e);   % The value of -(h_bar^2)/2m

TotalLength =1E-9;        % interval length
PrSize = 200;               % precision  

% potential wall
Wallenergy = 1E10; %
WallWidth = 30;

x = linspace(-TotalLength,TotalLength,PrSize);
dx = (x(2)-x(1))/2;  

% potential
V(1:PrSize) = 0.0;
V(PrSize-WallWidth:PrSize) = Wallenergy;
V(1:WallWidth) = Wallenergy;


for i=WallWidth: PrSize-WallWidth
    V(i) = (1/(4*pi*eps0))*(qe*qp)/(abs(i-100)*dx); % Coulomb potential
    if V(i) == -Inf % if 1/x == inf
        V(i) = V(i-1);
    end
end


% Hamiltonian
H = zeros(PrSize,PrSize);

for i = 2 : PrSize-1
   H(i,i)   = -2.0*c/dx^2 + V(i);
   H(i,i-1) = c/dx^2;
   H(i,i+1) = c/dx^2;
end


%Left side:  Psi(0)=0, forward difference approximation 
H(1,1) = c/dx^2 + V(1);
H(1,2) = -2.0*c/dx^2;
H(1,3) = c/dx^2;


%Right side: Psi(PrSize+1)=0
H(PrSize,PrSize)   = c/dx^2 + V(PrSize);
H(PrSize,PrSize-1) =-2.0*c/dx^2;
H(PrSize,PrSize-2) = c/dx^2;

%eigenvalue-problem
[Psi,Emx] = eig(H);

%one-dimensional list
for i = 1:PrSize
   E(i) = Emx(i,i);
end

%Finds the minimum-energy eigenvalue
[MinE,MinEindex] = min(E);
MinE

Psi0 = Psi(:,MinEindex);
Psi0 = Psi0./sqrt(sum(abs(Psi0).^2*dx));
E0 = MinE;

%Finds the second minimum-energy eigenvalue
E(MinEindex) = E(MinEindex)+Wallenergy;
[MinE,MinEindex] = min(E);
MinE

Psi1 = Psi(:,MinEindex);
Psi1 = Psi1./sqrt(sum(abs(Psi1).^2*dx));
E1 = MinE;


%Finds the third minimum-energy eigenvalue
E(MinEindex) = E(MinEindex)+Wallenergy;
[MinE,MinEindex] = min(E);
MinE

Psi2 = Psi(:,MinEindex);
Psi2 = Psi2./sqrt(sum(abs(Psi2).^2*dx));
E2 = MinE;

%Finds the fourth minimum-energy eigenvalue
E(MinEindex) = E(MinEindex)+Wallenergy;
[MinE,MinEindex] = min(E);
MinE

Psi3 = Psi(:,MinEindex);
Psi3 = Psi3./sqrt(sum(abs(Psi3).^2*dx));
E3 = MinE;


figure;
title('|\Psi(x)|^2');
hold on;
plot(x(1:PrSize),V(1:PrSize),'r--');
plot(x,abs(Psi0).^2,'g');
plot(x,abs(Psi1).^2,'c');
plot(x,abs(Psi2).^2,'b');
plot(x,abs(Psi3).^2,'k');
xlabel("x");
ylabel("|\Psi(x)|^2");
legend('Potential',strcat('Lowest Energy state E=',num2str(E0/qp),'eV'),strcat('2nd lowest Energy state E=',num2str(E1/qp),'eV'), strcat('3rd lowest Energy state E=',num2str(E2/qp),'eV'),... 
    strcat('4th lowest Energy state E=',num2str(E3/qp),'eV'));
axis ([-1E-9 1E-9 0 3E10]);
hold off;

figure;
title('\Psi(x)');
hold on;
plot(x(1:PrSize),V(1:PrSize),'r--');
plot(x,Psi0,'g');
plot(x,Psi1,'c');
plot(x,Psi2,'b');
plot(x,Psi3,'k');
xlabel("x");
ylabel("\Psi(x)");
legend('Potential',strcat('Lowest Energy state E=',num2str(E0/qp),'eV'),strcat('2nd lowest Energy state E=',num2str(E1/qp),'eV'), strcat('3rd lowest Energy state E=',num2str(E2/qp),'eV'),... 
    strcat('4th lowest Energy state E=',num2str(E3/qp),'eV'));
axis ([-1E-9 1E-9 -10E4 3E5]);
hold off;

figure;
plot(V(80:120))